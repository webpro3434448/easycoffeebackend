/* eslint-disable prettier/prettier */


export class CreateOrderDto {
    orderItems: {
        productId: number;
        qty: number;
    }[];
    userId: number;
}